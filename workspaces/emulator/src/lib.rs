//! Emulator
//!
//! This crate goes over the BFCPU emulator, which defined user-interfaces and an environment in which to run BFCPU code.

#![deny(
    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]

use std::{
    collections::HashMap,
    error::Error,
    fmt::{Debug, Display},
};

const CODE_TO_CHAR: [char; 16] = [
    '>', '<', '+', '-', '[', ']', '.', ',', '@', '^', '*', '~', '&', '#', '}', '{',
];
const PAGE_SIZE: usize = u16::MAX as usize + 1;
const BLANK_PAGE: [u16; PAGE_SIZE] = [0u16; PAGE_SIZE];

/// A BFCPU instruction.
/// Only uses the first 4 bits of the byte.
#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub struct Inst(u8);

impl Inst {
    /// Creates a new instance of an instruction by taking only the first 4 bits as the instruction.
    pub fn new(i: u8) -> Self {
        Self(i & 0xF)
    }

    /// Returns an instruction from it's character representation.
    pub fn from_char(c: char) -> Option<Self> {
        Some(Self(match c {
            '>' => 0,
            '<' => 1,
            '+' => 2,
            '-' => 3,
            '[' => 4,
            ']' => 5,
            '.' => 6,
            ',' => 7,
            '@' => 8,
            '^' => 9,
            '*' => 10,
            '~' => 11,
            '&' => 12,
            '#' => 13,
            '}' => 14,
            '{' => 15,
            _ => return None,
        }))
    }

    /// Returns the character associated with the instruction.
    pub fn to_char(&self) -> char {
        CODE_TO_CHAR[self.0 as usize]
    }

    /// Gets the code associated with the instruction.
    pub fn code(&self) -> u8 {
        self.0
    }
}

impl From<Inst> for char {
    fn from(i: Inst) -> Self {
        i.to_char()
    }
}

impl Debug for Inst {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.to_char())
    }
}

impl Display for Inst {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.to_char())
    }
}

/// Error returned while attempting to interpret a BFCPU program.
#[derive(Debug, Hash, Copy, Clone, PartialEq, Eq)]
pub enum InterpreterInitError {
    /// The given code has a left bracket without a matching right bracket at the given location.
    NoMatchingRightJump(usize),

    /// The given code has a right bracket without a matching left bracket at the given location.
    NoMatchingLeftJump(usize),

    /// The given code has a non-parsible instruction character at the given location.
    NonInstructionCharacter(usize),
}

impl Display for InterpreterInitError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            Self::NoMatchingRightJump(i) => {
                write!(
                    f,
                    "no matching right jump for left jump at {} while initializing interpreter",
                    i
                )
            }
            Self::NoMatchingLeftJump(i) => {
                write!(
                    f,
                    "no matching left jump for right jump at {} while initializing interpreter",
                    i
                )
            }
            Self::NonInstructionCharacter(i) => {
                write!(f, "non-parsible character at {} in program", i)
            }
        }
    }
}

impl Error for InterpreterInitError {}

/// Notices that occure during the runtime of the interpreter.
/// The interpreter will still be able to run further commands even when a notice is returned.
/// The notices are returned in case the user decides that certain program behavior is invalid.
#[derive(Debug, Hash, Copy, Clone, PartialEq, Eq)]
pub enum InterpreterRunNotice {
    /// A memory address was accessed outside the range of pages.
    InvalidMemoryAccess {
        /// Instruction pointer at time of access.
        ip: usize,
        /// Data pointer at time of access.
        dp: (u16, u16),
        /// Instruction at time of access.
        inst: Inst,
    },

    /// A memory address was mutated unsuccessfully outside of the range of pages.
    InvalidMemoryMut {
        /// Instruction pointer at time of access.
        ip: usize,
        /// Data pointer at time of access.
        dp: (u16, u16),
        /// Instruction at time of access.
        inst: Inst,
    },
}

impl Display for InterpreterRunNotice {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidMemoryAccess { ip, dp, inst } => {
                write!(
                    f,
                    "invalid memory read from addr {}, page {} while executing {} at {}",
                    dp.0, dp.1, inst, ip
                )
            }
            Self::InvalidMemoryMut { ip, dp, inst } => {
                write!(
                    f,
                    "invalid memory write to addr {}, page {} while executing {} at {}",
                    dp.0, dp.1, inst, ip
                )
            }
        }
    }
}

impl Error for InterpreterRunNotice {}

/// BFCPU interpreter.
#[derive(Debug)]
pub struct Interpreter {
    ip: usize,
    dp: (u16, u16),
    prog: Vec<Inst>,
    reg: u16,
    data: Vec<Option<[u16; PAGE_SIZE]>>,
    jumpmap: HashMap<usize, usize>,
    // TODO: Peripherals
}

impl Interpreter {
    /// Creates a new interpreter to run the given BFCPU code.
    /// Note that the indices given by the returned [`InterpreterError`]s ignore non-instruction characters.
    ///
    /// # Errors
    ///
    /// The constructor will throw an error if there are invalid characters in the code iterator, or if the `[` and `]` brackets don't match up.
    ///
    /// # Iterator Behavior
    ///
    /// The function will attempt to go through the entirety of the given code character iterator, unless an error is found (such as an invalid character).
    pub fn new(
        code: impl Iterator<Item = char>,
        pages: usize,
    ) -> Result<Self, InterpreterInitError> {
        let prog: Vec<Inst> = code
            .map(Inst::from_char)
            .enumerate()
            .map(|(l, i)| i.ok_or(InterpreterInitError::NonInstructionCharacter(l)))
            .collect::<Result<Vec<Inst>, InterpreterInitError>>()?;

        let data = vec![None; pages];
        let ip = 0;
        let dp = (0, 0);
        let reg = 0;
        let mut l_stack = Vec::new();
        let mut jumpmap = HashMap::new();

        for (l, i) in prog.iter().enumerate() {
            match i.code() {
                4 => {
                    l_stack.push(l);
                }
                5 => {
                    if let Some(jump) = l_stack.pop() {
                        jumpmap.insert(jump, l + 1);
                        jumpmap.insert(l, jump);
                    } else {
                        return Err(InterpreterInitError::NoMatchingRightJump(l));
                    }
                }
                _ => {}
            }
        }

        if let Some(jump) = l_stack.pop() {
            return Err(InterpreterInitError::NoMatchingLeftJump(jump));
        }

        Ok(Self {
            ip,
            dp,
            prog,
            reg,
            data,
            jumpmap,
        })
    }

    /// Gets a reference to a given page if the ask is in bounds.
    pub fn get_page(&self, pn: u16) -> Option<&[u16; PAGE_SIZE]> {
        let pn = pn as usize;
        if pn >= self.data.len() {
            return None;
        }
        if let Some(page) = &self.data[pn] {
            Some(page)
        } else {
            Some(&BLANK_PAGE)
        }
    }

    /// Gets the instruction pointer.
    pub fn iptr(&self) -> usize {
        self.ip
    }

    /// Gets the data pointer.
    /// The first value is the address in the page, the second value is the page number.
    pub fn dptr(&self) -> (u16, u16) {
        self.dp
    }

    /// Number of pages allowed for use.
    pub fn pages(&self) -> usize {
        self.data.len()
    }

    /// Gets the register value.
    pub fn register(&self) -> u16 {
        self.reg
    }

    /// Writes the given value to the location in memory pointed at by the data pointer.
    fn write_val(&mut self, val: u16, inst: Inst) -> Result<(), InterpreterRunNotice> {
        let pn = self.dp.1 as usize;
        if pn >= self.data.len() {
            return Err(InterpreterRunNotice::InvalidMemoryMut {
                ip: self.ip,
                dp: self.dp,
                inst,
            });
        }

        if !self.data[pn].is_some() {
            let page = [0u16; PAGE_SIZE];
            self.data[pn] = Some(page);
        }
        self.data[pn].expect("pre-filled page missing")[self.dp.0 as usize] = val;
        Ok(())
    }

    /// Reads the given value from the location in memory pointer at by the data pointer.
    fn read_val(&mut self, inst: Inst) -> Result<u16, InterpreterRunNotice> {
        let pn = self.dp.1 as usize;
        if pn >= self.data.len() {
            return Err(InterpreterRunNotice::InvalidMemoryAccess {
                ip: self.ip,
                dp: self.dp,
                inst,
            });
        }

        if let Some(page) = &self.data[pn] {
            Ok(page[self.dp.0 as usize])
        } else {
            Ok(0)
        }
    }

    /// Increments the program pointer.
    fn ip_inc(&mut self) {
        self.ip += 1;
    }

    /// Applies an in-place operation to the data pointer to by the data pointer.
    fn in_place_fn(
        &mut self,
        op: fn(&Self, u16) -> u16,
        inst: Inst,
    ) -> Result<(), InterpreterRunNotice> {
        let val = self.read_val(inst);
        if val.is_err() {
            self.ip_inc();
        }
        let val = val?;
        let res = self.write_val(op(&self, val), inst);
        self.ip_inc();
        res
    }

    // >
    #[inline]
    fn dp_inc(&mut self) -> Result<(), InterpreterRunNotice> {
        self.dp.0 = self.dp.0.overflowing_add(1).0;
        self.ip_inc();
        Ok(())
    }

    // <
    #[inline]
    fn dp_dec(&mut self) -> Result<(), InterpreterRunNotice> {
        self.dp.0 = self.dp.0.overflowing_sub(1).0;
        self.ip_inc();
        Ok(())
    }

    // +
    #[inline]
    fn data_add(&mut self) -> Result<(), InterpreterRunNotice> {
        fn inc(_: &Interpreter, val: u16) -> u16 {
            val.overflowing_add(1).0
        }
        self.in_place_fn(inc, Inst::new(2))
    }

    // -
    #[inline]
    fn data_sub(&mut self) -> Result<(), InterpreterRunNotice> {
        fn dec(_: &Interpreter, val: u16) -> u16 {
            val.overflowing_sub(1).0
        }
        self.in_place_fn(dec, Inst::new(3))
    }

    // [
    #[inline]
    fn jump_left(&mut self) -> Result<(), InterpreterRunNotice> {
        let res = self.read_val(Inst::new(4)).map(|v| v > 0);
        let jump = (&res).unwrap_or(false);
        if jump {
            self.ip = *self
                .jumpmap
                .get(&self.ip)
                .expect("jumpmap doesn;t contain entry for [");
        } else {
            self.ip_inc();
        }
        res?;
        Ok(())
    }

    // ]
    #[inline]
    fn jump_right(&mut self) -> Result<(), InterpreterRunNotice> {
        self.ip = *self
            .jumpmap
            .get(&self.ip)
            .expect("jumpmap doesn't contain entry for ]");
        Ok(())
    }

    // .
    #[inline]
    fn input(&mut self) -> Result<(), InterpreterRunNotice> {
        // TODO: Finish input implementation
        self.ip_inc();
        Ok(())
    }

    // ,
    #[inline]
    fn output(&mut self) -> Result<(), InterpreterRunNotice> {
        // TODO: Finish output implementation
        self.ip_inc();
        Ok(())
    }

    // @
    #[inline]
    fn jump_addr(&mut self) -> Result<(), InterpreterRunNotice> {
        let res = self.read_val(Inst::new(8));
        let addr = (&res).unwrap_or(0);
        self.dp.0 = addr;
        self.ip_inc();
        res?;
        Ok(())
    }

    // ^
    #[inline]
    fn reg_copy(&mut self) -> Result<(), InterpreterRunNotice> {
        let res = self.read_val(Inst::new(9));
        let val = (&res).unwrap_or(0);
        self.reg = val;
        self.ip_inc();
        res?;
        Ok(())
    }

    // *
    #[inline]
    fn reg_paste(&mut self) -> Result<(), InterpreterRunNotice> {
        let res = self.write_val(self.reg, Inst::new(10));
        self.ip_inc();
        res
    }

    // ~
    #[inline]
    fn shift_left(&mut self) -> Result<(), InterpreterRunNotice> {
        fn rot(_: &Interpreter, val: u16) -> u16 {
            val.rotate_left(1)
        }
        self.in_place_fn(rot, Inst::new(11))
    }

    // &
    #[inline]
    fn nand(&mut self) -> Result<(), InterpreterRunNotice> {
        fn nand(intp: &Interpreter, val: u16) -> u16 {
            !(intp.reg & val)
        }
        self.in_place_fn(nand, Inst::new(12))
    }

    // #
    #[inline]
    fn jump_page(&mut self) -> Result<(), InterpreterRunNotice> {
        let res = self.read_val(Inst::new(13));
        let page = (&res).unwrap_or(0);
        self.dp.1 = page;
        self.ip_inc();
        res?;
        Ok(())
    }

    // }
    #[inline]
    fn page_inc(&mut self) -> Result<(), InterpreterRunNotice> {
        self.dp.1 = self.dp.1.overflowing_add(1).0;
        self.ip_inc();
        Ok(())
    }

    // {
    #[inline]
    fn page_dec(&mut self) -> Result<(), InterpreterRunNotice> {
        self.dp.1 = self.dp.1.overflowing_sub(1).0;
        self.ip_inc();
        Ok(())
    }

    /// Runs a step of execution.
    pub fn next(&mut self) -> Result<bool, InterpreterRunNotice> {
        if let Some(inst) = self.prog.get(self.ip) {
            match inst.code() {
                0 => self.dp_inc(),
                1 => self.dp_dec(),
                2 => self.data_add(),
                3 => self.data_sub(),
                4 => self.jump_left(),
                5 => self.jump_right(),
                6 => self.input(),
                7 => self.output(),
                8 => self.jump_addr(),
                9 => self.reg_copy(),
                10 => self.reg_paste(),
                11 => self.shift_left(),
                12 => self.nand(),
                13 => self.jump_page(),
                14 => self.page_inc(),
                15 => self.page_dec(),
                _ => panic!("invalid instruction attempted to be executed"),
            }?;
            Ok(true)
        } else {
            Ok(false)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{Inst, Interpreter, InterpreterInitError};

    /// Tests that instructions created with [`new`](Inst::new) will always yield valid codes.
    #[test]
    fn inst_valid_codes() {
        for c in 0..=255u8 {
            assert!(Inst::new(c).code() < 16u8);
        }
    }

    /// Tests that the interpreter instantiates correctly when given a valid code snippet.
    #[test]
    fn interpreter_init_success() -> Result<(), InterpreterInitError> {
        let code = "&*{*[&{~*@#{<#,+-^<.,&~*{>]<{.";
        Interpreter::new(code.chars(), 10)?;
        Ok(())
    }

    /// Tests that the interpreter throws an error when there is a non-matching left bracket.
    #[test]
    fn interpreter_unmatched_left() {
        let code = "--,^><,@#,}{.@[>,+][#[-^#&^-*.";
        let res = Interpreter::new(code.chars(), 10);
        assert!(matches!(res.unwrap_err(), InterpreterInitError::NoMatchingLeftJump(_)));
    }

    /// Tests that the interpreter throws an error when there is a non-matching right bracket.
    #[test]
    fn interpreter_unmatched_right() {
        let code = "--,^><,@#,}{.@[>,+]]#]-^#&^-*.";
        let res = Interpreter::new(code.chars(), 10);
        assert!(matches!(res.unwrap_err(), InterpreterInitError::NoMatchingRightJump(_)));
    }

    /// Tests that the interpreter throws an error when there is an invalid instruction.
    #[test]
    fn interpreter_invalid_character() {
        let code = "[].*+,+<,>,~@c-[>{>]#{..<~.>&.@";
        let res = Interpreter::new(code.chars(), 10);
        assert!(matches!(res.unwrap_err(), InterpreterInitError::NonInstructionCharacter(_)));
    }

    /// Tests that non-branching, non-illegal memory access code runs without notices and in the expected amound of time.
    #[test]
    fn interpreter_run_no_branch() {
        let code = "}@,}.^,,,^+,~@{<+**}{#}@.&{<-.";
        let mut intp = Interpreter::new(code.chars(), 10).expect("interpreter failed to parse valid code");
        for i in 0..30 {
            assert_eq!(intp.next(), Ok(true), "interpreter failed to run program as expected at instruction {}", i);
        }
        assert_eq!(intp.next(), Ok(false), "interpreter failed to stop program execution at expected time");
    }
}
